#include "XOPStandardHeaders.h"			// Include ANSI headers, Mac headers, IgorXOP.h, XOP.h and XOPSupport.h
#include "GaGeXOP.h"
#include "CsAppSupport.h"
#include "CsSDKMisc.h"
#include "CsTchar.h"
#include "CsPrototypes.h"
#include <chrono>

// Global Variables
static int	gCallSpinProcess = 1;		// Set to 1 to all user abort (cmd dot) and background processing.
int			gGageDataFolderID = 0;  //Used to store dataFolderID during csInitialize for use elsewhere
Handle		gAcquisitionWaveHandle;
Handle		gChannel1WaveHandle;
Handle		gChannel2WaveHandle;
Handle		gTrigger1WaveHandle;
CSHANDLE	gBoardHandle;

static int
gageIgorError(int csErr)
{
	int igorErr;

	if (csErr<0)
	{
		igorErr = -csErr + FIRST_XOP_ERR + 4;
	}
	else if (csErr == 0 || csErr == 1) {
		igorErr = 0;
	}
	else {
		igorErr = csErr + FIRST_XOP_ERR;
	}

	return(igorErr);
}
#pragma pack(2)	// All structures passed to Igor are two-byte aligned.

struct GageDoAddParams {					// We receive this structure from Igor when our operation is invoked.
	// parameter defs go here

	double		action;
	double		result;

};
typedef struct GCDoAddParams GCDoAddParams;
#pragma pack()

/*
Usage: GageDo(int)
CsDo commands that are available with following int:
1	Transfers configuration setting values from the drivers to the CompuScope hardware.
	Incorrect values will not be transferred and an error code will be returned.
2	Starts acquisition
3	Emulates a trigger event
4	Aborts an acquisition or transfer
5	Invokes on-board auto-calibration sequence
6	Resets the system
7	Transfers configuration setting values from the drivers to the CompuScope hardware
8	Resets the time-stamp counter
*/

static int
GageDo(GageDoAddParams* p)
{
	int32				i32Status = CS_SUCCESS;

	i32Status = CsDo(gBoardHandle, p->action);

	p->result = i32Status;
	return(gageIgorError(p->result));
}

static int
gage_Util_CsGet(CSHANDLE whichSystem, int whichConfig, int whichConfigType, waveHndl whichWave)
{
	int32				i32Status = CS_SUCCESS;
	int32				gageConfig;
	long				igorIdx[MAX_DIMENSIONS]; // Identifies the point of interest
	double				igorVals[2]; // Value returned here
	int					result;

	CSCHANNELCONFIG		channelPtr = { 0 };
	CSACQUISITIONCONFIG	acquisitionPtr = { 0 };
	CSTRIGGERCONFIG		triggerPtr = { 0 };

	switch (whichConfig)
	{
	case 0:
		acquisitionPtr.u32Size = sizeof(CSACQUISITIONCONFIG);
		gageConfig = CS_ACQUISITION;
		break;

	case 1:
		channelPtr.u32Size = sizeof(CSCHANNELCONFIG);
		channelPtr.u32ChannelIndex = 1;
		gageConfig = CS_CHANNEL;
		break;

	case 2:
		channelPtr.u32Size = sizeof(CSCHANNELCONFIG);
		channelPtr.u32ChannelIndex = 2;
		gageConfig = CS_CHANNEL;
		break;

	case 3:
		triggerPtr.u32Size = sizeof(CSTRIGGERCONFIG);
		triggerPtr.u32TriggerIndex = 1;
		gageConfig = CS_TRIGGER;
		break;
	}

	if (whichConfig == 0) {  //acquisition config

		i32Status = CsGet(whichSystem, gageConfig, whichConfigType, &acquisitionPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));

		igorIdx[0] = 0;
		igorVals[0] = acquisitionPtr.u32Size;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 1;
		igorVals[0] = acquisitionPtr.i64SampleRate;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 2;
		igorVals[0] = acquisitionPtr.u32ExtClk;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 3;
		igorVals[0] = acquisitionPtr.u32ExtClkSampleSkip;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 4;
		igorVals[0] = acquisitionPtr.u32Mode;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 5;
		igorVals[0] = acquisitionPtr.u32SampleBits;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 6;
		igorVals[0] = acquisitionPtr.i32SampleRes;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 7;
		igorVals[0] = acquisitionPtr.u32SampleSize;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 8;
		igorVals[0] = acquisitionPtr.u32SegmentCount;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 9;
		igorVals[0] = acquisitionPtr.i64Depth;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 10;
		igorVals[0] = acquisitionPtr.i64SegmentSize;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 11;
		igorVals[0] = acquisitionPtr.i64TriggerTimeout;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 12;
		igorVals[0] = acquisitionPtr.u32TrigEnginesEn;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 13;
		igorVals[0] = acquisitionPtr.i64TriggerDelay;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 14;
		igorVals[0] = acquisitionPtr.i64TriggerHoldoff;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 15;
		igorVals[0] = acquisitionPtr.i32SampleOffset;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 16;
		igorVals[0] = acquisitionPtr.u32TimeStampConfig;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;

		WaveHandleModified(whichWave);


	}
	else if (whichConfig == 1 || whichConfig == 2)  //channel config
	{
		i32Status = CsGet(whichSystem, gageConfig, whichConfigType, &channelPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));


		igorIdx[0] = 0;
		igorVals[0] = channelPtr.u32Size;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 1;
		igorVals[0] = channelPtr.u32ChannelIndex;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 2;
		igorVals[0] = channelPtr.u32Term;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 3;
		igorVals[0] = channelPtr.u32InputRange;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 4;
		igorVals[0] = channelPtr.u32Impedance;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 5;
		igorVals[0] = channelPtr.u32Filter;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 6;
		igorVals[0] = channelPtr.i32DcOffset;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 7;
		igorVals[0] = channelPtr.i32Calib;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;

		WaveHandleModified(whichWave);

	}
	else {  //trigger config
		i32Status = CsGet(whichSystem, gageConfig, whichConfigType, &triggerPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));


		igorIdx[0] = 0;
		igorVals[0] = triggerPtr.u32Size;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 1;
		igorVals[0] = triggerPtr.u32TriggerIndex;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 2;
		igorVals[0] = triggerPtr.u32Condition;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 3;
		igorVals[0] = triggerPtr.i32Level;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 4;
		igorVals[0] = triggerPtr.i32Source;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 5;
		igorVals[0] = triggerPtr.u32ExtCoupling;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 6;
		igorVals[0] = triggerPtr.u32ExtTriggerRange;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 7;
		igorVals[0] = triggerPtr.u32ExtImpedance;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 8;
		igorVals[0] = triggerPtr.i32Value1;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 9;
		igorVals[0] = triggerPtr.i32Value2;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 10;
		igorVals[0] = triggerPtr.u32Filter;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		igorIdx[0] = 11;
		igorVals[0] = triggerPtr.u32Relation;
		if (result = MDSetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;

		WaveHandleModified(whichWave);

	}

	return i32Status;

}


/* gage_Util_CsSet(CSHANDLE whichSystem,int whichConfig,int whichConfigType)
function that sets the requested system info from into the gage driver.
whichConfig 0=Acquisition, 1=CH1,2=CH2,3=Trigger1
*/
static int
gage_Util_CsSet(CSHANDLE whichSystem, int whichConfig, waveHndl whichWave)
{
	int32				i32Status = CS_SUCCESS;
	int32				gageConfig;
	long				igorIdx[MAX_DIMENSIONS]; // Identifies the point of interest
	double				igorVals[2]; // Value returned here
	int					result;
	char				noticeStr[50];
	CSCHANNELCONFIG		channelPtr = { 0 };
	CSACQUISITIONCONFIG	acquisitionPtr = { 0 };
	CSTRIGGERCONFIG		triggerPtr = { 0 };

	switch (whichConfig)
	{
	case 0:
		acquisitionPtr.u32Size = sizeof(CSACQUISITIONCONFIG);
		gageConfig = CS_ACQUISITION;
		break;

	case 1:
		channelPtr.u32Size = sizeof(CSCHANNELCONFIG);
		channelPtr.u32ChannelIndex = 1;
		gageConfig = CS_CHANNEL;
		break;

	case 2:
		channelPtr.u32Size = sizeof(CSCHANNELCONFIG);
		channelPtr.u32ChannelIndex = 2;
		gageConfig = CS_CHANNEL;
		break;

	case 3:
		triggerPtr.u32Size = sizeof(CSTRIGGERCONFIG);
		triggerPtr.u32TriggerIndex = 1;
		gageConfig = CS_TRIGGER;
		break;
	}

	if (whichConfig == 0) {  //acquisition config

		i32Status = CsGet(whichSystem, gageConfig, CS_CURRENT_CONFIGURATION, &acquisitionPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));

		igorIdx[0] = 1;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i64SampleRate = igorVals[0];

		igorIdx[0] = 2;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32ExtClk = igorVals[0];

		igorIdx[0] = 3;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32ExtClkSampleSkip = igorVals[0];

		igorIdx[0] = 4;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32Mode = igorVals[0];

		igorIdx[0] = 5;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32SampleBits = igorVals[0];

		igorIdx[0] = 6;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i32SampleRes = igorVals[0];

		igorIdx[0] = 7;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32SampleSize = igorVals[0];

		igorIdx[0] = 8;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32SegmentCount = igorVals[0];

		igorIdx[0] = 9;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i64Depth = igorVals[0];
		sprintf(noticeStr, "depth: %I64i \r", acquisitionPtr.i64Depth);
		XOPNotice(noticeStr);
		igorIdx[0] = 10;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i64SegmentSize = igorVals[0];
		sprintf(noticeStr, "seg size: %I64i \r", acquisitionPtr.i64SegmentSize);
		XOPNotice(noticeStr);
		igorIdx[0] = 11;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i64TriggerTimeout = igorVals[0];
		
		igorIdx[0] = 12;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32TrigEnginesEn = igorVals[0];

		igorIdx[0] = 13;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i64TriggerDelay = igorVals[0];

		igorIdx[0] = 14;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i64TriggerHoldoff = igorVals[0];
		sprintf(noticeStr, "Trigger Holdoff %I64i \r", acquisitionPtr.i64TriggerHoldoff);
		XOPNotice(noticeStr);

		igorIdx[0] = 15;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.i32SampleOffset = igorVals[0];

		igorIdx[0] = 16;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		acquisitionPtr.u32TimeStampConfig = igorVals[0];


		i32Status = CsSet(whichSystem, gageConfig, &acquisitionPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));

	}
	else if (whichConfig == 1 || whichConfig == 2)  //channel config
	{

		i32Status = CsGet(whichSystem, gageConfig, CS_CURRENT_CONFIGURATION, &channelPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));

		igorIdx[0] = 2;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		channelPtr.u32Term = igorVals[0];
		igorIdx[0] = 3;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		channelPtr.u32InputRange = igorVals[0];
		igorIdx[0] = 4;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		channelPtr.u32Impedance = igorVals[0];
		igorIdx[0] = 5;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		channelPtr.u32Filter = igorVals[0];
		igorIdx[0] = 6;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		channelPtr.i32DcOffset = igorVals[0];
		igorIdx[0] = 7;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		channelPtr.i32Calib = igorVals[0];

		i32Status = CsSet(whichSystem, gageConfig, &channelPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));

	}
	else {  //trigger config

		i32Status = CsGet(whichSystem, gageConfig, CS_CURRENT_CONFIGURATION, &triggerPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));

		igorIdx[0] = 2;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.u32Condition = igorVals[0];
		igorIdx[0] = 3;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.i32Level = igorVals[0];
		igorIdx[0] = 4;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.i32Source = igorVals[0];
		igorIdx[0] = 5;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.u32ExtCoupling = igorVals[0];
		igorIdx[0] = 6;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.u32ExtTriggerRange = igorVals[0];
		igorIdx[0] = 7;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.u32ExtImpedance = igorVals[0];
		igorIdx[0] = 8;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.i32Value1 = igorVals[0];
		igorIdx[0] = 9;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.i32Value2 = igorVals[0];
		igorIdx[0] = 10;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.u32Filter = igorVals[0];
		igorIdx[0] = 11;
		if (result = MDGetNumericWavePointValue(whichWave, igorIdx, igorVals))
			return result;
		triggerPtr.u32Relation = igorVals[0];

		i32Status = CsSet(whichSystem, gageConfig, &triggerPtr);
		if (CS_FAILED(i32Status))
			return (gageIgorError(i32Status));

	}

	return i32Status;

}
#pragma pack(2)		// All structures passed to Igor are two-byte aligned
struct GageInitializeParams
{
	double result;
};
typedef struct GageInitializeParams GageInitializeParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
GageInitialize(GageInitializeParams* p)	// See the top of the file for instructions on how to invoke this function from Igor Pro 3.0 or later.
{
	int				    i32Status = CS_SUCCESS;
	CSHANDLE			hSystem = 0;
	CSSYSTEMINFO		CsSysInfo = { 0 };
	int					SetCurrentDataFolder(DataFolderHandle dataFolderH);
	long				refNum = 0;
	int					result = 0;
	DataFolderHandle	rootDataFolderHPtr;
	DataFolderHandle	packageDataFolderH;
	DataFolderHandle	currentFolderH;
	char*				newDataFolderName;
	DataFolderHandle	newDataFolderHPtr;

	int					MakeWave(waveHndl* waveHandlePtr, const char* waveName, long numPoints, int type, int overwrite);
	//int					MDSetDimensionLabel(waveHndl wavH, int dim, long el, char lab[MAX_DIM_LABEL_CHARS + 1]);
	int					MDSetDimensionLabel(waveHndl wavH, int dim, long el, char lab[255 + 1]);
	waveHndl			AcquisitionHandlePtr; // Place to put waveHndl for new wave
	char*				AcquisitionWaveName = "CSACQUISITIONCONFIG\0"; // C string containing name of Igor wave
	long				AcquisitionPoints = 17; // Desired length of wave
	waveHndl			Channel1HandlePtr; // Place to put waveHndl for new wave
	char*				Channel1WaveName = "CSCHANNELCONFIG1\0"; // C string containing name of Igor wave
	long				ChannelPoints = 8; // Desired length of wave
	waveHndl			Channel2HandlePtr; // Place to put waveHndl for new wave
	char*				Channel2WaveName = "CSCHANNELCONFIG2\0"; // C string containing name of Igor wave
	waveHndl			TriggerHandlePtr; // Place to put waveHndl for new wave
	char*				TriggerWaveName = "CSTRIGGERCONFIG\0"; // C string containing name of Igor wave
	long				TriggerPoints = 12; // Desired length of wave
	int					WaveType = NT_I32; // Numeric type (e.g., NT_FP32, NT_FP64)
	int					OW = 0; // If non-zero, overwrites existing wave
	/*
	Initializes the CompuScope boards found in the system. If the
	system is not found a message with the error code will appear.
	Otherwise i32Status will contain the number of systems found.
	*/
	if (gBoardHandle>0)
		CsFreeSystem(gBoardHandle);

	i32Status = CsInitialize();

	if (CS_FAILED(i32Status))
	{
		p->result = i32Status;
		return (gageIgorError(p->result));
	}
	/*
	Get System. This sample program only supports one system. If
	2 systems or more are found, the first system that is found
	will be the system that will be used. hSystem will hold a unique
	system identifier that is used when referencing the system.
	*/
	i32Status = CsGetSystem(&hSystem, 0, 0, 0, 0);

	if (CS_FAILED(i32Status))
	{
		p->result = i32Status;
		return (gageIgorError(p->result));
	}

	gBoardHandle = hSystem;

	/*
	Get System information. The u32Size field must be filled in
	prior to calling CsGetSystemInfo
	*/
	CsSysInfo.u32Size = sizeof(CSSYSTEMINFO);
	i32Status = CsGetSystemInfo(hSystem, &CsSysInfo);

	/*
	Now we have a handle to the cs board, create a new data folder in Igor and
	populate it with the system info in 3 different waves.


	*/
	if (result = GetCurrentDataFolder(&currentFolderH))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}

	newDataFolderName = "GageCS\0";
	// find the root folder
	if (result = GetRootDataFolder(0, &rootDataFolderHPtr))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}

	if (result = GetNamedDataFolder(rootDataFolderHPtr, ":Packages:", &packageDataFolderH))
	{
		result = NewDataFolder(rootDataFolderHPtr, newDataFolderName, &newDataFolderHPtr);
	}
	else {
		result = NewDataFolder(packageDataFolderH, newDataFolderName, &newDataFolderHPtr);
	}


	// Create a new folder under the root.

	if (result)
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}

	// Get and store the GageCS data folderID as a global var for later use.
	if (result = GetDataFolderIDNumber(newDataFolderHPtr, &gGageDataFolderID))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}

	// Set the GageCS folder as the current data folder.
	SetCurrentDataFolder(newDataFolderHPtr);

	// Make the 4 waves that correspond to the 4 Gage configuration data structures
	if (result = MakeWave(&AcquisitionHandlePtr, AcquisitionWaveName, AcquisitionPoints, WaveType, OW))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}
	gAcquisitionWaveHandle = AcquisitionHandlePtr;
	if (result = MakeWave(&Channel1HandlePtr, Channel1WaveName, ChannelPoints, WaveType, OW))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}
	gChannel1WaveHandle = Channel1HandlePtr;
	if (result = MakeWave(&Channel2HandlePtr, Channel2WaveName, ChannelPoints, WaveType, OW))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}
	gChannel2WaveHandle = Channel2HandlePtr;
	if (result = MakeWave(&TriggerHandlePtr, TriggerWaveName, TriggerPoints, WaveType, OW))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}
	gTrigger1WaveHandle = TriggerHandlePtr;

	//Set the dimension labels for all 3 waves
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 0, "Size\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 1, "SampleRate\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 2, "ExtClk\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 3, "ExtClkSampleSkip\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 4, "Mode\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 5, "SampleBits\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 6, "SampleRes\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 7, "SampleSize\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 8, "SegmentCount\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 9, "Depth\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 10, "SegmentSize\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 11, "TriggerTimeout\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 12, "TrigEnginesEn\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 13, "TriggerDelay\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 14, "TriggerHoldoff\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 15, "SampleOffset\0");
	MDSetDimensionLabel(AcquisitionHandlePtr, 0, 16, "TimeStampConfig\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 0, "Size\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 1, "ChannelIndex\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 2, "Term\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 3, "InputRange\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 4, "Impedance\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 5, "Filter\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 6, "DcOffset\0");
	MDSetDimensionLabel(Channel1HandlePtr, 0, 7, "Calib\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 0, "Size\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 1, "ChannelIndex\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 2, "Term\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 3, "InputRange\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 4, "Impedance\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 5, "Filter\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 6, "DcOffset\0");
	MDSetDimensionLabel(Channel2HandlePtr, 0, 7, "Calib\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 0, "Size\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 1, "TriggerIndex\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 2, "Condition\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 3, "Level\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 4, "Source\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 5, "ExtCoupling\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 6, "ExtTriggerRange\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 7, "ExtImpedance\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 8, "Value1\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 9, "Value2\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 10, "Filter\0");
	MDSetDimensionLabel(TriggerHandlePtr, 0, 11, "Relation\0");

	//Populate the 4 waves with the data from the 3 data structures using the configuration 
	//currently stored in the hardware.
	i32Status = gage_Util_CsGet(hSystem, 0, CS_CURRENT_CONFIGURATION, AcquisitionHandlePtr);
	i32Status = gage_Util_CsGet(hSystem, 1, CS_CURRENT_CONFIGURATION, Channel1HandlePtr);
	i32Status = gage_Util_CsGet(hSystem, 2, CS_CURRENT_CONFIGURATION, Channel2HandlePtr);
	i32Status = gage_Util_CsGet(hSystem, 3, CS_CURRENT_CONFIGURATION, TriggerHandlePtr);


	if (result = SetCurrentDataFolder(currentFolderH))
	{
		CsFreeSystem(hSystem);
		p->result = result;
		return result;
	}

	p->result = i32Status;

	// return result

	return 0;
}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned
struct GageAcquireParams{

	double result;
};
typedef struct GageAcquireParams GageAcquireParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
GageAcquire(GageAcquireParams* p)
{
	int32				i32Status = CS_SUCCESS;

	i32Status = CsDo(gBoardHandle, 2);
	p->result = i32Status;
	return 0;
}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned
struct GageWaitParams {
	double timeout;
	double result;
};
typedef struct GageWaitParams GageWaitParams;
#pragma pack()		// Reset structure alignment to default.

static int
GageWait(GageWaitParams* p)
{
	int32 i32Status;
	int u = 0;
	double timeout = p->timeout;
	char noticeStr[50];
	/*
	Wait until the acquisition is complete.
	*/
	i32Status = CsGetStatus(gBoardHandle);
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds;

	double totaltime = 0;

	while (!(ACQ_STATUS_READY == i32Status))
	{
		i32Status = CsGetStatus(gBoardHandle);
		if (u % 100 == 0)
		{
			SpinProcess();
		}
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end - start;
		totaltime = elapsed_seconds.count();
		if (totaltime > timeout)
		{
			CsFreeSystem(gBoardHandle);
			return gageIgorError(-46);
		}
	}

	return 0;
}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned
struct GageSetParams {
	double config;
	double result;
};
typedef struct GageSetParams GageSetParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
GageSet(GageSetParams* p)
{
	int32				i32Status = CS_SUCCESS;
	CSHANDLE			hSystem = 0;
	int					result = 0;
	int					tempConfig;
	char				noticeStr[50];
	tempConfig = p->config;
	//call gage_Util_CsGet

	switch (tempConfig)
	{
	case 0:
		result = gage_Util_CsSet(gBoardHandle, p->config, gAcquisitionWaveHandle);

		break;
	case 1:
		result = gage_Util_CsSet(gBoardHandle, p->config, gChannel1WaveHandle);
		break;
	case 2:
		result = gage_Util_CsSet(gBoardHandle, p->config, gChannel2WaveHandle);

		break;
	case 3:
		result = gage_Util_CsSet(gBoardHandle, p->config, gTrigger1WaveHandle);
		break;

	default:
		result = gage_Util_CsSet(gBoardHandle, 0, gAcquisitionWaveHandle);
		result = gage_Util_CsSet(gBoardHandle, 1, gChannel1WaveHandle);
		result = gage_Util_CsSet(gBoardHandle, 2, gChannel2WaveHandle);
		result = gage_Util_CsSet(gBoardHandle, 3, gTrigger1WaveHandle);
		i32Status = CsDo(gBoardHandle, ACTION_COMMIT_COERCE);
		if (CS_FAILED(i32Status))
		{
			p->result = i32Status;
			return (gageIgorError(p->result));
		}
	}

	if (result>1)
	{
		p->result = result;
		return p->result;
	}


	p->result = i32Status;
	return 0;
}


/* 
GageTest is used in Igor to debug Wave accessing features only.


*/
#pragma pack(2)		// All structures passed to Igor are two-byte aligned
struct GageTestParams {
	waveHndl whichWave;
	double whichChannel;
	double result;
};

extern "C" int
GageTest(GageTestParams* p)
{
	char noticeStr[50];
	sprintf(noticeStr, "test output %f \r", p->whichChannel);
	XOPNotice(noticeStr);

//	float* fptr;
	//fptr = (float*)WaveData(p->whichWave);
	sprintf(noticeStr, "error 3.case0\r");
	XOPNotice(noticeStr);


	if (p->whichWave == NULL) {
		XOPNotice("null wave op");
		return NULL_WAVE_OP;
	}
	CountInt dimensionSizes[MAX_DIMENSIONS + 1];
	MemClear(dimensionSizes, sizeof(dimensionSizes));
	dimensionSizes[0] = 100;
	dimensionSizes[1] = 0;
	sprintf(noticeStr, "error 3.case1\r");
	XOPNotice(noticeStr);
	MDChangeWave(p->whichWave, NT_I8 | NT_UNSIGNED, dimensionSizes);
	sprintf(noticeStr, "error 3.case2\r");
	XOPNotice(noticeStr);

	MDChangeWave(p->whichWave, NT_I16, dimensionSizes);
	sprintf(noticeStr, "error 3.case3\r");
	XOPNotice(noticeStr);

	MDChangeWave(p->whichWave, NT_I32 | NT_UNSIGNED, dimensionSizes);
	sprintf(noticeStr, "error 3.case4\r");
	XOPNotice(noticeStr);

	p->result = 2 * p->whichChannel;
	return 0;
}

#pragma pack(2)		// All structures passed to Igor are two-byte aligned
struct GageTransferParams {
	waveHndl whichWave;
	double whichChannel;
	double result;
};
typedef struct GageTransferParams GageTransferParams;
#pragma pack()		// Reset structure alignment to default.

extern "C" int
GageTransfer(GageTransferParams* p)
{
	int32						i32Status = CS_SUCCESS;
	int							result = 0;
	IN_PARAMS_TRANSFERDATA		inParams = { 0 };
	OUT_PARAMS_TRANSFERDATA		outParams = { 0 };
	CSACQUISITIONCONFIG			CsAcqCfg = { 0 };
	CSCHANNELCONFIG				CsChannelCfg = { 0 };
	void*						pBuffer = NULL;
	float*						pVBuffer = NULL;
	int							startAddress = 0;
	int							xferLength = 0;
	int							i = 0;
	double*						igorPtr;
	long						numbytes, column, row;
	long						dataOffset;
	long						loopDataOffset;
	int							hState;
	long						dimensionSizes[MAX_DIMENSIONS + 1];
	float*						numBuffer;
	double						scaleFactor;
	char						noticeStr[50];
	float						elapsedTime;
	clock_t						t1, t2;
	
	int whichChannel = p->whichChannel;
	//int whichChannel = 1;

	/*
	Get the current sample size, resolution and offset parameters from the driver
	by calling CsGet for the ACQUISTIONCONFIG structure. These values are used
	when saving the file.
	*/

	CsChannelCfg.u32Size = sizeof(CSCHANNELCONFIG);
	CsChannelCfg.u32ChannelIndex = whichChannel;
	i32Status = CsGet(gBoardHandle, CS_CHANNEL, CS_ACQUIRED_CONFIGURATION, &CsChannelCfg);
	if (CS_FAILED(i32Status))
	{
		p->result = i32Status;
		CsFreeSystem(gBoardHandle);

		return (gageIgorError(p->result));
	}

	CsAcqCfg.u32Size = sizeof(CSACQUISITIONCONFIG);
	i32Status = CsGet(gBoardHandle, CS_ACQUISITION, CS_ACQUIRED_CONFIGURATION, &CsAcqCfg);
	if (CS_FAILED(i32Status))
	{
		p->result = i32Status;
		CsFreeSystem(gBoardHandle);

		return (gageIgorError(p->result));
	}


	startAddress = CsAcqCfg.i64Depth - CsAcqCfg.i64SegmentSize;
	xferLength = CsAcqCfg.i64SegmentSize;

	MemClear(dimensionSizes, sizeof(dimensionSizes));
	dimensionSizes[0] = xferLength; // rows
	dimensionSizes[1] = CsAcqCfg.u32SegmentCount; // columns
	dimensionSizes[2] = 0; // layers OR zero marks the first unused dimension

	switch (CsAcqCfg.u32SampleSize){
	case 1:
		MDChangeWave(p->whichWave, NT_I8 | NT_UNSIGNED, dimensionSizes);
		break;
	case 2:
		MDChangeWave(p->whichWave, NT_I16, dimensionSizes);
		break;
	case 4:
		MDChangeWave(p->whichWave, NT_I32 | NT_UNSIGNED, dimensionSizes);
		break;
	}

	inParams.u16Channel = whichChannel;
	inParams.i64Length = xferLength;
	inParams.i64StartAddress = startAddress;
	inParams.u32Mode = TxMODE_DEFAULT;

	// Ensures we can access the wave (result=0 if successful)
	if (result = MDAccessNumericWaveData(p->whichWave, kMDWaveAccessMode0, &dataOffset))
	{
		p->result = result;
		return result;
	}

	t1 = clock();
	int spinclock = 0;
	/*
	Transfer the data into pBuffer
	*/
	for (i = 0; i<CsAcqCfg.u32SegmentCount; i++)
	{
		if (i == 0) {
			loopDataOffset = 0;
		}
		else {
			loopDataOffset = loopDataOffset + 2 * outParams.i64ActualLength;
		}
		pBuffer = (void*)((char*)(*p->whichWave) + (dataOffset + loopDataOffset)); // DEREFERENCE
		inParams.pDataBuffer = pBuffer;
		inParams.u32Segment = (i + 1);
		i32Status = CsTransfer(gBoardHandle, &inParams, &outParams);

		if (CS_FAILED(i32Status))
		{

			p->result = i32Status;
			if (NULL != pBuffer)
				VirtualFree(pBuffer, 0, MEM_RELEASE);
			return (gageIgorError(p->result));
		}

		// Allow background processing and spin the beachball!
		if (spinclock % 100 == 0)
		{
			SpinProcess();
		}
		spinclock++;
	}


	t2 = clock();
	elapsedTime = ((float)(t2 - t1) / (float)CLOCKS_PER_SEC);
	//sprintf(noticeStr, "Data transfer time from Channel %f.0 is %f seconds.\r", p->whichChannel, elapsedTime);
	sprintf(noticeStr, "Data transfer time is %f seconds.\r", elapsedTime);
	XOPNotice(noticeStr);


	//	Need to scale the data here based on the AcqCfg sampleResolution field
	// to do so we must first unlock the handle, change the wave to FP_64
	//then reopen the handle and apply the scaleFactor

	// Ensures we can change the wave (result=0 if successful, i.e. the if-block is skipped)
	if (result = MDChangeWave(p->whichWave, NT_FP32, dimensionSizes))
	{
		p->result = result;
		return result;
	}

	// Sets up direct writing of the wave (result=0 if successful)
	if (result = MDAccessNumericWaveData(p->whichWave, kMDWaveAccessMode0, &dataOffset))
	{
		p->result = result;
		return result;
	}

	numBuffer = (float*)((char*)(*p->whichWave) + dataOffset); //set pointer to wave handle

	scaleFactor = ((double)CsChannelCfg.u32InputRange / (2000 * abs((double)CsAcqCfg.i32SampleRes)));

	//loop through entire buffer and apply scaleFactor
	for (column = 0; column<CsAcqCfg.u32SegmentCount; column++) {
		for (row = 0; row<xferLength; row++) {
			*numBuffer++ *= scaleFactor;
		}
	}
	
	p->result = i32Status;

	return 0;

}



/*	RegisterFunction()
	
	Igor calls this at startup time to find the address of the
	XFUNCs added by this XOP. See XOP manual regarding "Direct XFUNCs".
*/
static XOPIORecResult
RegisterFunction()
{
	int funcIndex;

	funcIndex = (int)GetXOPItem(0);		// Which function is Igor asking about?
	switch (funcIndex) {
		case 0:						// WAGetWaveInfo(wave)
			return (XOPIORecResult)GageInitialize;
			break;
		case 1:						
			return (XOPIORecResult)GageAcquire;
			break;
		case 2:
			return (XOPIORecResult)GageWait;
			break;
		case 3:
			return (XOPIORecResult)GageSet;
			break;
		case 4:
			return (XOPIORecResult)GageTransfer;
			break;
		case 5:
			return (XOPIORecResult)GageDo;
			break;
		case 6:
			return (XOPIORecResult)GageTest;
			break;
	}
	return 0;
}

/*	DoFunction()
	
	Igor calls this when the user invokes one if the XOP's XFUNCs
	if we returned NIL for the XFUNC from RegisterFunction. In this
	XOP, we always use the direct XFUNC method, so Igor will never call
	this function. See XOP manual regarding "Direct XFUNCs".
*/
static int
DoFunction()
{
	int funcIndex;
	void *p;				// Pointer to structure containing function parameters and result.
	int err;

	funcIndex = (int)GetXOPItem(0);	// Which function is being invoked ?
	p = (void*)GetXOPItem(1);		// Get pointer to params/result.
	switch (funcIndex) {
		case 0:						
			err = GageInitialize((GageInitializeParams*)p);
			break;
		case 1:						
			err = GageAcquire((GageAcquireParams*)p);
			break;
		case 2:
			err = GageWait((GageWaitParams*)p);
			break;
		case 3:
			err = GageSet((GageSetParams*)p);
			break;
		case 4:
			err = GageTransfer((GageTransferParams*)p);
			break;
		case 5:
			err = GageDo((GageDoAddParams*)p);
			break;
		case 6:
			err = GageTest((GageTestParams*)p);
			break;

	}
	return(err);
}

/*	XOPEntry()

	This is the entry point from the host application to the XOP for all messages after the
	INIT message.
*/
extern "C" void
XOPEntry(void)
{	
	XOPIORecResult result = 0;

	switch (GetXOPMessage()) {
		case FUNCTION:						// Our external function being invoked ?
			result = DoFunction();
			break;

		case FUNCADDRS:
			result = RegisterFunction();
			break;
	}
	SetXOPResult(result);
}

/*	main(ioRecHandle)

	This is the initial entry point at which the host application calls XOP.
	The message sent by the host must be INIT.
	main() does any necessary initialization and then sets the XOPEntry field of the
	ioRecHandle to the address to be called for future messages.
*/
HOST_IMPORT int
main(IORecHandle ioRecHandle)
{	
	XOPInit(ioRecHandle);				// Do standard XOP initialization.
	SetXOPEntry(XOPEntry);				// Set entry point for future calls.
	
	if (igorVersion < 600) {			// Requires Igor Pro 6.00 or later.
		SetXOPResult(OLD_IGOR);			// OLD_IGOR is defined in GaGeXOP.h and there are corresponding error strings in GaGeXOP.r and GaGeXOPWinCustom.rc.
		return EXIT_FAILURE;
	}

	SetXOPResult(0L);
	return EXIT_SUCCESS;
}




