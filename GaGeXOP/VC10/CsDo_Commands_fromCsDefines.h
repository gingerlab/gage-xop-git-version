//! \brief Transfers configuration setting values from the drivers to the CompuScope hardware
//!
//! Transfers configuration setting values from the drivers to the CompuScope hardware.<BR>
//! Incorrect values will not be transferred and an error code will be returned.
//! \sa ACTION_COMMIT_COERCE
#define ACTION_COMMIT		1

//! \brief Starts acquisition
//!
//! Starts acquisition
#define	ACTION_START		2

//! \brief Emulates a trigger event
//!
//! Emulates a trigger event
#define ACTION_FORCE	3

//! \brief Aborts an acquisition or a transfer
//!
//! Aborts an acquisition or a transfer
#define	ACTION_ABORT	4

//! \brief Invokes CompuScope on-board auto-calibration sequence
//!
//! Invokes CompuScope on-board auto-calibration sequence
#define	ACTION_CALIB	5

//! \brief Reset the system.
//!
//! Resets the CompuScope system to its default state (the state after initialization). All pending operations will be terminated
#define ACTION_RESET	6

//! \brief Transfers configuration setting values from the drivers to the CompuScope hardware
//!
//! Transfers configuration setting values from the drivers to the CompuScope hardware.<BR>
//! Incorrect values will be coerced to valid available values, which will be transferred to the CompuScope system.
//! If coercion was required CS_CONFIG_CHANGED is returned
//! \sa ACTION_COMMIT
#define ACTION_COMMIT_COERCE	7

//! \brief Resets the time-stamp counter
//!
//! Resets the time-stamp counter
#define ACTION_TIMESTAMP_RESET	8

//! \}
// CSDO_ACTIONS